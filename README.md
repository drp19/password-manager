# A Rust Password Manager

**WARNING: Don't use this for super secret stuff, I think it works correctly but encryption is hard**

I have attempted to implement encryption correctly, but it is easily possible that there is some weird thing that leaks data that I didn't catch, because that's how encryption works. This is just a fun project for a class, and I am not responsible for any data or privacy loss as a result of using this. Though, since it doesn't use any sort of internet connectivity, at worst this isn't any more problematic than a simple text file of your passwords, which isn't *necessarily* catastrophic.

## Why
In short: it was a class project. However, it was very open ended and most of the work put into this was just because I liked doing it.

There a lot of password managers out there, and a lot of the most common ones use cloud storage in some capacity. While this makes it easier to use, it does add a layer of required trust. Even though most/all use strong encryption and security protocols, the actual mechanism of these is often hidden. (NOTE: I'm not saying you *shouldn't* trust these providers, just that it does require some level of trust).

By storing all data locally and in a *single file*, this application allows you to know where your data is being stored, and share it across devices relatively easily in whatever way works best. 
- NOTE: As of now, the app doesn't listen for file modifications, so if you have one file synced across devices, updates to the file on one device will not be reflected on the other until you restart the program, and will be overwritten if further modifications are made. Be careful.

There also just aren't a lot of command line password managers around (BitWarden has a good one), and if you need a portable and non-connected solution, this type of program may be useful.

## Features
- Creating and saving an encrypted password database that is stored entirely locally.
- Data is compressed using the DEFLATE algorithm, which means the file on disk is very small.
- Currently supports searching, adding, and deleting basic items.
- A nice custom TUI.
- Modular file structure that allows for versioning and migration.
    - Versions before commit `a9c179b4` are not compatible due to a security-related change.
- Pretty good Unicode support (multibyte characters, case insensitive search)

## Security
All data stored to disk is encrypted using AES-256-GCM. 
- Uses the [aes-gcm](https://docs.rs/aes-gcm/0.8.0/aes_gcm/) crate, which has been independently audited.
- The nonce is regenerated with a CSPRNG every time the file is written to in order to avoid duplicate values, and is stored unencrypted in the file.
    - The generator is reseeded each time the program is started using system randomness, and then it is stored in memory and reused for subsequent saves.

The master password is hashed using the scrypt algorithm ([crate](https://docs.rs/scrypt/0.6.4/scrypt/index.html)) with N=2^16, and salted using a randomly generated value that is stored unencrypted in the file.
- This hardness value is likely secure enough for most applications, but in the future this may support changing this value.
- This algorithm is similar to PBKDF2, but provides additional difficulty by using memory.

The master password is only stored in memory for a short period during the decryption process, which somewhat protects against cold-boot attacks, though if you are in an environment where that is a concern, you probably shouldn't be using this anyway.
- The decryption key is stored securely using the [secrets](https://crates.io/crates/secrets) crate, which prevents buffer overreads/writes, and stops the OS from saving to swap.
- Account passwords are also stored with the secrets crate, which is intented to prevent the OS from swapping to disk (though the memory usage is pretty small and is unlikely to happen anyway).

Like any password manager, if your device is compromised, without native hardware integration with a TPM or other deliberate hardening, software security doesn't help much. The intention is that all data stored on disk by this application is encrypted, only storing unencrypted data in memory. 


## Compilation
TLDR: Compile with:
- Linux/Mac: `RUSTFLAGS="-Ctarget-cpu=sandybridge -Ctarget-feature=+aes,+sse2,+sse4.1,+ssse3" cargo build --release; strip /target/release/password_manager`
- Windows: `set RUSTFLAGS=-Ctarget-cpu=sandybridge -Ctarget-feature=+aes,+sse2,+sse4.1,+ssse3`, then `cargo build --release`
- Run with `cargo run --release` or run the file `target/release/password_manager(.exe)`
- Requires `gcc` and `make` to be installed. 

ALWAYS USE the `release` BUILD!! Debug compilation mode leads to the scrypt key generation taking 10-50x as long as it should (no, that doesn't increase security). There are several compilation options in `Cargo.toml` that are used to decrease binary size but increase compile time, so disable these if you are developing (but still use release).

The `RUSTFLAGS` enable native AES performance. If for some reason you're using this on a very old CPU, this may cause issues.

If you want debug information, set `RUST_LOG=debug`, otherwise only info and up will be printed.

The `secrets` crate requires `libsodium`, a C library that provides memory access functions. By default, this is linked statically through the `libsodium-sys` crate, but if you would rather use your own installation of it, `SODIUM_SHARED=1` and `SODIUM_USE_PKG_CONFIG=1` should be set.

This has been tested against `x86_64-pc-windows-msvc`, `x86_64-apple-darwin`, `x86_64-unknown-linux-gnu` and 
`x86_64-unknown-linux-musl`. Statically linking with `musl` only barely increases binary size, but doesn't provide much benefit. 