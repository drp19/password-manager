extern crate rpassword;
use lazy_static::lazy_static;
use crossterm::{cursor::*, style::*, event::KeyCode, event::{KeyEvent, KeyModifiers, read}};
use std::{collections::HashSet, io, path::Path};
use std::io::prelude::*;
use storage::{DB, DBRow};
use tracing::{Level, error, warn};
use secrets::SecretVec;
use unicode_segmentation::UnicodeSegmentation;

mod storage;

const USE_TTY: bool = true;
const SEARCH_ROWS: u16 = 4;

lazy_static! {
    static ref EXIT_KEYS: HashSet<KeyEvent> = {
        let mut m = HashSet::new();
        m.insert(KeyEvent {
            code: KeyCode::Esc,
            modifiers: KeyModifiers::NONE
        });
        m.insert(KeyEvent {
            code: KeyCode::Char('d'),
            modifiers: KeyModifiers::CONTROL
        });
        m.insert(KeyEvent {
            code: KeyCode::Char('c'),
            modifiers: KeyModifiers::CONTROL
        });
        m
    };
}

/// Based off of the execute! macro in crossterm
#[macro_export]
macro_rules! safe_execute {
    ($($command:expr),*) => {{
        let mut stdout = ::std::io::stdout();
        // Queue each command, then flush
        
        if let Err(e) = 
        crossterm::queue!(stdout $(, $command)*)
        .map_err(|e| e.to_string())
        .and_then(|()| stdout.flush().map_err(|e| e.to_string())) {
            println!("{}", e);
            exit(1)
        }
    }}
}

fn exit(code: i32) {
    if let Err(e) = crossterm::terminal::disable_raw_mode() {
        println!("Could not enable raw mode: {}", e);
    }
    println!("\nExiting...");
    std::process::exit(code);
}

fn enable_raw() {
    if let Err(e) = crossterm::terminal::enable_raw_mode() {
        
        println!("Could not enable raw mode: {}", e);
        exit(1);
    }
}

fn disable_raw() {
    if let Err(e) = crossterm::terminal::disable_raw_mode() {
        println!("Could not enable raw mode: {}", e);
        exit(1);
    }
}

fn main() {
    if std::env::var("RUST_LOG").is_err() {
        tracing_subscriber::fmt().with_max_level(Level::INFO).init();
    } else {
        tracing_subscriber::fmt().with_max_level(Level::DEBUG).init();
    }
    enable_raw();
    safe_execute!(crossterm::style::Print("Press 1 for a new file, or 2 to open an existing file: "));
    let mut db: DB = loop {
        let event = read().unwrap();
        match event {
            crossterm::event::Event::Key(c) if c.code == KeyCode::Char('1') => {
                disable_raw();
                println!("");
                let res = new();
                if res.is_none() {
                    println!("Could not save new DB.");
                    exit(1);
                }
                break res.unwrap();
            }
            crossterm::event::Event::Key(c) if c.code == KeyCode::Char('2') => {
                disable_raw();
                println!("");
                let res = open();
                if res.is_none() {
                    println!("Could not open DB.");
                    exit(1);
                }
                break res.unwrap();
            }
            crossterm::event::Event::Key(c) if EXIT_KEYS.contains(&c) => {
                exit(1);
            }
            _ => {  
                //safe_execute!(crossterm::style::Print('\n'), MoveToNextLine(1),crossterm::style::Print(format!("Unknown Key {:?}", event)));

            }
        }
    };
    enable_raw();
    safe_execute!(SetForegroundColor(crossterm::style::Color::Green), Print("Welcome to your vault!\n"), SetForegroundColor(crossterm::style::Color::Reset), MoveToNextLine(1));
    let mut print_inst = true;
    loop {
        if print_inst {
            safe_execute!(Print("-----\n"), MoveToNextLine(1),
                    Print("Press 1 to search,\n"), MoveToNextLine(1), MoveToColumn(7),
                    Print("2 to insert,\n"), MoveToNextLine(1), MoveToColumn(7),
                    Print("3 to delete,\n"), MoveToNextLine(1), MoveToColumn(7),
                    Print("4 to exit.\n"), MoveToNextLine(1));
        }
        print_inst = true;
        let event = read().unwrap();
        match event {
            crossterm::event::Event::Key(c) if c.code == KeyCode::Char('1') => {
                safe_execute!(MoveToColumn(1), crossterm::terminal::ScrollDown(5), crossterm::terminal::Clear(crossterm::terminal::ClearType::FromCursorDown), 
                            SetForegroundColor(crossterm::style::Color::Green), Print("-"), SetForegroundColor(crossterm::style::Color::Reset),
                                        crossterm::style::Print("Search\n"), MoveToNextLine(1), Print("Use Up/Down to select and Enter/Tab to submit.\n"), MoveToNextLine(1));
                search(&db);
            }
            crossterm::event::Event::Key(c) if c.code == KeyCode::Char('2') => {
                safe_execute!(MoveToColumn(1), crossterm::terminal::ScrollDown(5), crossterm::terminal::Clear(crossterm::terminal::ClearType::FromCursorDown), 
                SetForegroundColor(crossterm::style::Color::Green), Print("-"), SetForegroundColor(crossterm::style::Color::Reset),
                            crossterm::style::Print("Insert\n"), MoveToNextLine(1));
                disable_raw();
                let insert_res = insert(&mut db);
                enable_raw();
                if insert_res.is_some() {
                    let row = insert_res.unwrap();
                    safe_execute!(crossterm::terminal::ScrollDown(6), crossterm::terminal::Clear(crossterm::terminal::ClearType::FromCursorDown),
                    MoveToNextLine(1), Print(format!("{}:{}\n", row.get_site_string(true), row.get_uname_string(true))), MoveToNextLine(1));
                }
            }
            crossterm::event::Event::Key(c) if c.code == KeyCode::Char('3') => {
                safe_execute!(MoveToColumn(1), crossterm::terminal::ScrollDown(5), crossterm::terminal::Clear(crossterm::terminal::ClearType::FromCursorDown), 
                SetForegroundColor(crossterm::style::Color::Green), Print("-"), SetForegroundColor(crossterm::style::Color::Reset),
                            crossterm::style::Print("Delete\n"), MoveToNextLine(1), Print("Search & Select the item to delete.\n"), MoveToNextLine(1));
                let res = search(&db).clone();
                if res.is_some() {
                    safe_execute!(SetForegroundColor(crossterm::style::Color::Red), Print("Are you sure? [y/N] "), SetForegroundColor(crossterm::style::Color::Reset));
                    let row = res.unwrap();
                    loop {
                        let del_event = read().unwrap();
                        match del_event {
                            crossterm::event::Event::Key(c) if c.code == KeyCode::Char('y') => {
                                disable_raw();
                                db.delete(row.get_id(), true);
                                enable_raw();
                                safe_execute!(MoveToColumn(1), crossterm::terminal::ScrollDown(3), crossterm::terminal::Clear(crossterm::terminal::ClearType::FromCursorDown), crossterm::style::Print(format!("Success {}:{}\n", row.get_site_string(true), row.get_uname_string(true))), MoveToNextLine(1));
                                break;
                            }
                            crossterm::event::Event::Key(_) => {
                                safe_execute!(MoveToColumn(1), crossterm::terminal::ScrollDown(3), crossterm::terminal::Clear(crossterm::terminal::ClearType::FromCursorDown), MoveToNextLine(1));
                                break;
                            }
                            _ => {}
                        }
                    }
                }
            }
            crossterm::event::Event::Key(c) if c.code == KeyCode::Char('4') => {
                exit(1);
            }
            crossterm::event::Event::Key(c) if EXIT_KEYS.contains(&c) => {
                exit(1);
            }
            _ => { 
                print_inst = false;
                //safe_execute!(crossterm::style::Print('\n'), MoveToNextLine(1), crossterm::style::Print(format!("Unknown Key {:?}", event)));
            }
        }
    }
}

fn insert(db: &mut DB) -> Option<DBRow> {
    let stdin = io::stdin();
    println!("Enter site: ");
    let mut site_in = String::new();
    if let Err(e) = stdin.lock().read_line(&mut site_in) {
        error!(error = %e, "Could not read from stdin!");
        return None;
    }
    site_in = site_in.trim().to_string();
    println!("Enter username: ");
    let mut username_in = String::new();
    if let Err(e) = stdin.lock().read_line(&mut username_in) {
        error!(error = %e, "Could not read from stdin!");
        return None;
    }
    username_in = username_in.trim().to_string();
    println!("Enter password: ");
    let mut pass_in = String::new();
    if let Err(e) = stdin.lock().read_line(&mut pass_in) {
        error!(error = %e, "Could not read from stdin!");
        return None;
    }
    pass_in = pass_in.trim().to_string();
    db.insert(None, if site_in == "" {None} else {Some(site_in)}, 
            if username_in == "" {None} else {Some(username_in)}, 
            if pass_in == "" {None} else {Some(pass_in)}, true)
}

fn search(db: &DB) -> Option<DBRow> {
    for i in 0..SEARCH_ROWS {
        safe_execute!(crossterm::style::Print(format!("{}\n",i+1)), MoveToNextLine(1));
    }
    let mut choice: i16 = 0;
    let mut rows: Vec<&DBRow> = Vec::new();
    let mut search = String::new();
    update_search(db, &"".to_string());
    loop {
        let event = read().unwrap();
        match event {
            crossterm::event::Event::Key(c) if EXIT_KEYS.contains(&c) => {
                safe_execute!(MoveToColumn(1), crossterm::terminal::ScrollDown(SEARCH_ROWS+1), crossterm::terminal::Clear(crossterm::terminal::ClearType::FromCursorDown),
                    MoveToNextLine(1));
                choice = 0;
                break;
            }
            crossterm::event::Event::Key(KeyEvent {code: KeyCode::Enter, ..}) | crossterm::event::Event::Key(KeyEvent {code: KeyCode::Tab, ..}) => {
                safe_execute!(MoveToColumn(1), crossterm::terminal::ScrollDown(SEARCH_ROWS+1), crossterm::terminal::Clear(crossterm::terminal::ClearType::FromCursorDown));
                if choice != 0 && rows.get((choice-1) as usize).is_some() {
                    let row = rows.get((choice-1) as usize).unwrap();
                    disable_raw();
                    println!("Site: {}\nUsername: {}\nPassword: {}", row.get_site_string(false), row.get_uname_string(false), row.get_pwd_string(&db));
                    enable_raw();
                } else {
                    disable_raw();
                    println!("Err: No entry selected.");
                    enable_raw();
                }
                break;
            }
            crossterm::event::Event::Key(KeyEvent {code: KeyCode::Up, ..}) => {
                choice = move_selected(choice, -1, rows.len() as u16, false);
            }
            crossterm::event::Event::Key(KeyEvent {code: KeyCode::Down, ..}) => {
                choice = move_selected(choice, 1, rows.len() as u16, false);
            }
            crossterm::event::Event::Key(KeyEvent {code: KeyCode::Char(c), modifiers: m}) 
                if !(m == KeyModifiers::ALT || m == KeyModifiers::CONTROL)
                => {
                    search.push(c);
                    safe_execute!(crossterm::style::Print(c));
                    let old_size = rows.len();
                    rows = update_search(db, &search);
                    let new_size = rows.len();
                    if new_size < old_size && choice as usize > new_size {
                        choice = move_selected(choice, new_size as i16 - choice, rows.len() as u16, true);
                    }
            }
            crossterm::event::Event::Key(KeyEvent {code: KeyCode::Delete, ..}) | crossterm::event::Event::Key(KeyEvent {code: KeyCode::Backspace, ..}) 
                => {
                    let mut new_search = String::new();
                    let mut graphemes = search.graphemes(true).peekable();
                    while let Some(c) = graphemes.next() {
                        if graphemes.peek().is_some() {
                            new_search.push_str(c);
                        }
                    }
                    safe_execute!(MoveToColumn(1), crossterm::terminal::Clear(crossterm::terminal::ClearType::CurrentLine), Print(&new_search));
                    search = new_search;
                    rows = update_search(db, &search);
                    if rows.len() == 0 {choice = move_selected(choice, -1*choice, rows.len() as u16, true);}
            }
            _ => {}
        }
    }
    rows.get((choice-1) as usize).map(|v| v.clone().clone())
}

// will be all lower case
fn update_search<'a>(db: &'a DB, search: &String) -> Vec<&'a DBRow> {
    let mut res = db.search(search);
    res.reverse();
    let mut strings: Vec<String> = res.iter().map(|row| format!("| {:<20}| {:<20}", row.get_site_string(true), row.get_uname_string(true))).collect();
    
    safe_execute!(SavePosition, MoveToColumn(3), MoveUp(SEARCH_ROWS));
    for _ in 0..SEARCH_ROWS {
        let str = strings.pop();
        if str.is_some() {
            safe_execute!(Print(str.unwrap()), MoveToColumn(3), MoveDown(1));
        } else {
            safe_execute!(Print(format!("| {:<20}| {:<20}", "--", "--")), MoveToColumn(3), MoveDown(1));
        }
    };
    safe_execute!(RestorePosition);
    res.reverse();
    res
}
// 0, +1 -> move to 1
// 0, -1 -> move to MAX
// 1, -1 -> nothing/0
// MAX, +1 -> nothing/0
fn move_selected(from: i16, diff: i16, max: u16, overrun: bool) -> i16 {
    let new:i16;
    if from == 0 {
        if diff > 0 { new = i16::min(max as i16, 1); } else { new = u16::min(SEARCH_ROWS, max) as i16; };
    } else {
        if from + diff >= 1 && from + diff <= u16::min(SEARCH_ROWS, max) as i16 {
            new = from + diff;
        } else {
            if overrun { new = 0; } else { new = from; }
        }
    } 
    if from != 0 {
        safe_execute!(SavePosition, MoveToColumn(2), MoveUp(((1+SEARCH_ROWS) as i16-from) as u16), Print(" "), RestorePosition);
    }
    if new != 0 {
        safe_execute!(SavePosition, MoveToColumn(2), MoveUp(((1+SEARCH_ROWS) as i16-new) as u16), SetForegroundColor(crossterm::style::Color::Red), Print("*"), SetForegroundColor(crossterm::style::Color::Reset), RestorePosition);
    }
    new
}

fn open() -> Option<DB> {
    let stdin = io::stdin();
    println!("Enter filename:");
    let mut name_in = String::new();
    if let Err(e) = stdin.lock().read_line(&mut name_in) {
        error!(error = %e, "Could not read from stdin!");
    }
    let file_name = name_in.trim();
    if !Path::new(&format!("storage/{}", file_name)).exists() {
        warn!("File does not exist");
        return None;
    }

    let pwd_str = if USE_TTY {
        rpassword::read_password_from_tty(Some("Enter your master password: ")).unwrap()
    } else {
        println!("Enter your master password: ");
        rpassword::read_password().unwrap()
    };

    let password = SecretVec::<u8>::new(pwd_str.len(), |s| {
        s.copy_from_slice(pwd_str.as_bytes());
    });

    let db = storage::DB::load(password, file_name.to_string());
    db
}

fn new() -> Option<DB> {
    let stdin = io::stdin();
    println!("Enter identifying name:");
    let mut name_in = String::new();
    if let Err(e) = stdin.lock().read_line(&mut name_in) {
        error!(error = %e, "Could not read from stdin!");
    }
    let file_name = name_in.trim();

    let password;
    loop {
        let pwd_str = if USE_TTY {
            rpassword::read_password_from_tty(Some("Enter a good master password: ")).unwrap()
        } else {
            println!("Enter a good master password: ");
            rpassword::read_password().unwrap()
        };

        let pwd_cmf_str = if USE_TTY {
            rpassword::read_password_from_tty(Some("Confirm password: ")).unwrap()
        } else {
            println!("Confirm password: ");
            rpassword::read_password().unwrap()
        };

        if pwd_str == pwd_cmf_str {
            password = SecretVec::<u8>::new(pwd_str.len(), |s| {
                s.copy_from_slice(pwd_str.as_bytes());
            });
            break;
        } else {
            println!("Passwords are not identical! Retry or quit with ^c");
        }
    }
    let mut db = storage::DB::new(password, file_name.to_string());
    if db.save() {Some(db)} else {None}
}  

