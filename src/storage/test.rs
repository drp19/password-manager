use rand::{distributions::Alphanumeric, thread_rng};
use secrets::SecretVec;
use tracing::Level;

use super::*;


// checks generated key against
// script n=2^16 = 65536, r=8, p=1
// password: test_pass
// salt: (123456) to LE bytes [0..12] (40e20100000000000000000000000000)
// [link](https://gchq.github.io/CyberChef/#recipe=Scrypt(%7B'option':'Hex','string':'40e20100000000000000000000000000'%7D,65536,8,1,32)&input=dGVzdF9wYXNz)

// checks generated file against format: (all LE)
// 4 byte version (00000000)
// 16 byte salt (same as above)
// 12 byte nonce (010000000000000000000000)
// 8 byte data segment length
// data segment as enc data + 16 byte GCM key
//
// format spec: [link](https://gchq.github.io/CyberChef/#recipe=Regular_expression('User%20defined','((?:%5C%5Cw%5C%5Cw)%7B4%7D)((?:%5C%5Cw%5C%5Cw)%7B16%7D)((?:%5C%5Cw%5C%5Cw)%7B12%7D)(?:%5C%5Cw%5C%5Cw)%7B4%7D((?:%5C%5Cw%5C%5Cw)%7B8%7D)(.*)((?:%5C%5Cw%5C%5Cw)%7B16%7D)',true,true,false,false,false,false,'List%20capture%20groups')&input=MDAwMDAwMDA0MGUyMDEwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAxMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMTUwMDAwMDAwMDAwMDAwMDI2N2IyNzcwZTUzOGFiOTY5M2VjYTA2ZDE0OGYzOWI4ZWVjMTQ5OGEzYg)
// 
// data segment is encrypted with AES-256-GCM with key from scrypt, nonce/iv from file, gcm tag from above, and salt as AAD
// the data itself is compressed with raw DEFLATE, and in this case, the data is just 8 bytes of the data size (0)
// 
// decrypt spec: https://gchq.github.io/CyberChef/#recipe=AES_Decrypt(%7B'option':'Hex','string':'30519b4ee7d73c96b6f5066c4197799f2b341a821d6f7cdf99a2bf96849669eb'%7D,%7B'option':'Hex','string':'d1ea309b77e1c56e0c980d84'%7D,'GCM','Hex','Hex',%7B'option':'Hex','string':'bb2845ecc0078e94de2c90162cdf8198'%7D,%7B'option':'Hex','string':'40e20100000000000000000000000000'%7D)From_Hex('Auto')Raw_Inflate(0,0,'Adaptive',false,false)To_Hex('Space',0)&input=N2ZjZWZhODgxMQ

#[test]
fn empty_vault() {
    tracing_subscriber::fmt().with_max_level(Level::DEBUG).init();
    let pwd_str = "test_pass";

    let salt = (123456 as u128).to_le_bytes();
    let mut seed = [0u8; 32];
    seed[0..16].copy_from_slice(&(123456 as u128).to_le_bytes());
    seed[16..32].copy_from_slice(&(123456 as u128).to_le_bytes());
    let password = SecretVec::<u8>::new(pwd_str.len(), |s| {
        s.copy_from_slice(pwd_str.as_bytes());
    });
    let mut db = DB::new_seeded(password, salt, seed, String::new()); 

    let key = Vec::from(db.key.borrow().as_ref());
    assert_eq!(key, vec!(0x30, 0x51, 0x9b, 0x4e, 0xe7, 0xd7, 0x3c, 0x96, 0xb6, 0xf5, 0x06, 0x6c, 0x41, 0x97, 0x79, 0x9f, 0x2b, 0x34, 0x1a, 0x82, 0x1d, 0x6f, 0x7c, 0xdf, 0x99, 0xa2, 0xbf, 0x96, 0x84, 0x96, 0x69, 0xeb));
    
    //let mut nonce = [0u8; 12];
    //ChaCha20Rng::from_seed(seed).fill_bytes(&mut nonce);

    let mut res = Vec::new();
    db.save_into(&mut res);
    assert_eq!(res, vec!(
        0x00,0x00,0x00,0x00,
        0x40,0xe2,0x01,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
        0xd1,0xea,0x30,0x9b,0x77,0xe1,0xc5,0x6e,0x0c,0x98,0x0d,0x84,
        0x15,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
        0x7f,0xce,0xfa,0x88,0x11,
        0xbb,0x28,0x45,0xec,0xc0,0x07,0x8e,0x94,0xde,0x2c,0x90,0x16,0x2c,0xdf,0x81,0x98));
}

// all of the comments from above apply, but this time there is data, and the data is in the format of:
// 8 byte number of stored elements
// for each element (1 for now):
// 4 byte ID
// for site, username, password:
// 1 byte isPresent, and if present,
// 8 byte string length
// len byte string data
//
// decrypt example: https://gchq.github.io/CyberChef/#recipe=AES_Decrypt(%7B'option':'Hex','string':'30519b4ee7d73c96b6f5066c4197799f2b341a821d6f7cdf99a2bf96849669eb'%7D,%7B'option':'Hex','string':'d1ea309b77e1c56e0c980d84'%7D,'GCM','Hex','Hex',%7B'option':'Hex','string':'8ae91d0313a47c53eb65b75c3b87a2bc'%7D,%7B'option':'Hex','string':'40e20100000000000000000000000000'%7D)From_Hex('Auto')Raw_Inflate(0,0,'Adaptive',false,false)To_Hex('Space',0)&input=MzE2OWJiODUxMWYzNzgwYTVlZmQ2N2E3YTAzY2IyNzAzODczNjI4MDg4OTQ5ZDhkYTZlYWI4MjM1N2RhMzgxNjNjMmJkYmI2Y2YwN2FkN2EwZjNjYzQ2NQ

// also tests that subsequent re-encryptions increase the nonce by 1 and therefore, changes the data
#[test]
fn single_item() {
    let pwd_str = "test_pass";

    let salt = (123456 as u128).to_le_bytes();
    let mut seed = [0u8; 32];
    seed[0..16].copy_from_slice(&(123456 as u128).to_le_bytes());
    seed[16..32].copy_from_slice(&(123456 as u128).to_le_bytes());

    let password = SecretVec::<u8>::new(pwd_str.len(), |s| {
        s.copy_from_slice(pwd_str.as_bytes());
    });
    let mut db = DB::new_seeded(password, salt, seed, String::new()); 
    db.insert(Some(2028463384), Some("site".to_string()), Some("uname".to_string()), Some("pwd".to_string()), false);

    //let mut nonce = [0u8; 12];
    //let mut rand = ChaCha20Rng::from_seed(seed);
    //rand.fill_bytes(&mut nonce);
    //rand.fill_bytes(&mut nonce);

    let mut res = Vec::new();
    db.save_into(&mut res);
    
    assert_eq!(res, vec!(
        0x00, 0x00, 0x00, 0x00, 
        0x40, 0xe2, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
        0xd1, 0xea, 0x30, 0x9b, 0x77, 0xe1, 0xc5, 0x6e, 0x0c, 0x98, 0x0d, 0x84, 
        0x3c, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
        0x31, 0x69, 0xbb, 0x85, 0x11, 0xf3, 0x78, 0x0a, 0x5e, 0xfd, 0x67, 0xa7, 0xa0, 0x3c, 0xb2, 0x70, 0x38, 0x73, 0x62, 0x80, 0x88, 0x94, 0x9d, 0x8d, 0xa6, 0xea, 0xb8, 0x23, 0x57, 0xda, 0x38, 0x16, 0x3c, 0x2b, 0xdb, 0xb6, 0xcf, 0x07, 0xad, 0x7a, 0x0f, 0x3c, 0xc4, 0x65, 
        0x8a, 0xe9, 0x1d, 0x03, 0x13, 0xa4, 0x7c, 0x53, 0xeb, 0x65, 0xb7, 0x5c, 0x3b, 0x87, 0xa2, 0xbc));
    
    res = Vec::new();
    db.save_into(&mut res);
    assert_eq!(res, vec!(
        0x00, 0x00, 0x00, 0x00, 
        0x40, 0xe2, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
        0x70, 0xbb, 0xa2, 0x87, 0xa4, 0x7a, 0x4f, 0x0f, 0xdb, 0xf1, 0x1e, 0x5e, 
        0x3c, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
        0xa8, 0xc9, 0x1b, 0x13, 0x3e, 0x8e, 0xce, 0x70, 0x69, 0x4e, 0xe2, 0x85, 0xe0, 0x5a, 0x00, 0x2d, 0x53, 0x3e, 0xef, 0x5b, 0x2e, 0x75, 0x49, 0x6c, 0x2b, 0xac, 0xe3, 0x0a, 0x37, 0x6d, 0xa6, 0x3b, 0x4b, 0xa9, 0xc2, 0x5c, 0x53, 0x10, 0x73, 0xe4, 0x22, 0x67, 0x8f, 0x93,
        0xcd, 0x5d, 0x9f, 0xeb, 0x85, 0x0a, 0xd0, 0xcc, 0xc7, 0xff, 0x7f, 0xd3, 0x77, 0x8d, 0x0b, 0xc2));
}

#[test]
fn big_db() {
    
    
    let pwd_str = "test_pass";
    let password = SecretVec::<u8>::new(pwd_str.len(), |s| {
        s.copy_from_slice(pwd_str.as_bytes());
    });
    let mut db = DB::new(password, String::new()); 
    let mut generated: Vec<(Option<String>, Option<String>, Option<String>)> = Vec::new();
    for _ in 0..1000000 {
        generated.push((Some(thread_rng()
        .sample_iter(&Alphanumeric)
        .take(15)
        .map(char::from)
        .collect()), Some(thread_rng()
        .sample_iter(&Alphanumeric)
        .take(8)
        .map(char::from)
        .collect()), Some(thread_rng()
        .sample_iter(&Alphanumeric)
        .take(20)
        .map(char::from)
        .collect())));
    }
    println!("Done generating.");
    for (a,b,c) in generated {
        db.insert(None, a, b, c, false);
    }
    println!("Done inserting.");
    let mut res = Vec::new();
    db.save_into(&mut res);

    assert_eq!(db.data.rows.len(), 1000000);
    assert!(db.search(&"a".to_string()).len() > 0);
}