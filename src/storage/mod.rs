use std::{fs::File};
use tracing::{debug, warn};
use serde::{Deserialize, Serialize, Serializer, ser::SerializeSeq};
use v1::V1;
use aes_gcm::{Aes256Gcm, NewAead, aead::{Aead, generic_array::GenericArray, Payload}};
use flate2::{Compression, write::DeflateEncoder};
use secrets::{SecretBox, SecretVec};
use rand::prelude::*;
use rand_chacha::ChaCha20Rng;

mod v1;
#[cfg(test)]
mod test;

/// The structure of the file - stores **encrypted** data, which is then decoded and made into a DB struct.
#[derive(Serialize, Deserialize)]
struct PassFile {
    metadata: Metadata,
    data: Vec<u8>
}

#[derive(Serialize, Deserialize)]
pub enum Metadata {
    V1([u8;16], [u8;12])
}

pub struct DB {
    data: DBData,
    salt: [u8; 16],
    rand: ChaCha20Rng, //rand from entropy, used to generate nonce value
    key: SecretBox::<[u8; 32]>,
    file: String
}

/// Stores actual data (stuff that should be encrypted)
struct DBData {
    rows: Vec<DBRow>,
    passwords: PassVec
}

/// A single database entry
#[derive(Clone, Serialize)]
pub struct DBRow {
    id: u32,
    site: Option<String>,
    uname: Option<String>,
    pwd: Pwd
}

struct PassVec {
    vec: SecretVec<u8>, 
    len: usize
}

#[derive(Serialize, Clone)]
struct Pwd {
    #[serde(skip_serializing)]
    loc: Option<(usize, usize)>,
    for_ser: Option<Vec<u8>>
}


impl PassVec {
    fn insert(&mut self, pwd: Vec<u8>) -> (usize, usize) {
        let old_len = self.len;
        let old_size = self.vec.len();
        if old_len + pwd.len() > old_size {
            self.vec = SecretVec::new(old_size*2, |f| {
                let old = self.vec.borrow();
                f[0..old_len].copy_from_slice(&old[0..old_len]);
            })
        }
        let mut pass_vec = self.vec.borrow_mut();
        pass_vec.as_mut()[old_len..old_len+pwd.len()].copy_from_slice(&pwd);
        self.len += pwd.len();
        (old_len, self.len)
    }


    fn get(& self, loc: Option<(usize, usize)>) -> Option<Vec<u8>> {
        loc.map(|(start, end)|
            if start <= end && end <= self.len {
                Some(Vec::from(&self.vec.borrow()[start..end]))
            } else {
                None
            }
        ).flatten()
    }
}

impl Serialize for DBData {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let mut seq = serializer.serialize_seq(Some(self.rows.len()))?;
        for row in &self.rows {
            let ser_pwd = row.pwd.for_ser(&self.passwords);
            let mut for_ser = DBRow { id: row.id, site: row.site.clone(), uname: row.uname.clone(), pwd: ser_pwd};
            seq.serialize_element(&for_ser)?;
            for_ser.pwd.un_ser();
        }
        seq.end()
    }
}

impl Pwd {
    fn for_ser(&self, passes: &PassVec) -> Pwd {
        Pwd {loc: self.loc, for_ser: passes.get(self.loc)}
    }

    fn un_ser(&mut self) {
        self.for_ser = match &mut self.for_ser {
            Some(v) => {
                for i in 0..v.len() {
                    v[i] = 0;
                }
                None
            },
            None => None
        }
    }

    fn new(loc: Option<(usize, usize)>) -> Self {
        Pwd{loc, for_ser: None}
    }
}

impl DBRow {
    fn new(id: u32, site: Option<String>, uname: Option<String>, pwd: Option<String>, pwd_vec: &mut PassVec) -> DBRow {
        let pass = match pwd {
            Some(s) => {
                let bytes = s.into_bytes();
                let loc = pwd_vec.insert(bytes);
                Some(loc)
            },
            None => None
        };
        DBRow {
            id, site, uname,
            pwd: Pwd::new(pass)
        }
    }
    
    pub fn get_id(&self) -> u32 {
        self.id
    }

    pub fn get_site_string(&self, truncate: bool) -> String {
        self.site.clone().map(|mut v| {
            if truncate {v.truncate(20);};
            v
        }).unwrap_or("--".to_string())
    }
    pub fn get_uname_string(&self, truncate: bool) -> String {
        self.uname.clone().map(|mut v| {
            if truncate {v.truncate(20);};
            v
        }).unwrap_or("--".to_string())
    }
    pub fn get_pwd_string(&self, db: &DB) -> String {
        match self.pwd.loc {
            Some(_) => {
                String::from_utf8(db.data.passwords.get(self.pwd.loc).unwrap()).unwrap()
            }
            None => "--".to_string()
        }
    }
}

impl From<V1> for DB {
    fn from(v1: V1) -> Self {
        let mut passwords = PassVec{vec:  SecretVec::zero((1+v1.data.rows.len())*10), len: 0};
        let rand = ChaCha20Rng::from_entropy();
        let rows = v1.data.rows.into_iter().map(|v| DBRow{id: v.id, site: v.site, uname: v.uname, pwd: v1_pwd_conv(v.pwd, &mut passwords)}).collect();
        DB {
            data: DBData {passwords, rows},
            salt: v1.salt,
            rand,
            key: v1.key,
            file: v1.file
        }
    }
}

fn v1_pwd_conv(v1: Option<Vec<u8>>, passes: &mut PassVec) -> Pwd {
    Pwd::new(v1.map(|v| passes.insert(v)))
}

impl DB {
    pub fn new(password: secrets::SecretVec<u8>, filename: String) -> DB {
        V1::new(password, None, filename).into()
    }

    // FOR TESTING ONLY
    #[allow(dead_code)]
    fn new_seeded(password: secrets::SecretVec<u8>, salt: [u8; 16], rand_seed: [u8; 32], filename: String) -> DB {
        let mut db: DB = V1::new(password, Some(salt), filename).into();
        db.rand = ChaCha20Rng::from_seed(rand_seed);
        db
    }

    fn save_into<W>(&mut self, into: &mut W) -> bool  
    where W: std::io::Write {
        debug!(rows = self.data.rows.len(), "Compressing data.");
        let mut e = DeflateEncoder::new(Vec::new(), Compression::default());
        if let Err(e) = bincode::serialize_into(&mut e, &self.data) {
            warn!(error = %e, "Error encoding data.");
        }
        let db_bytes = e.finish().unwrap();
    
        debug!(bytes = db_bytes.len(), "Encrypting data.");
        let mut nonce_bytes = [0u8; 12];
        self.rand.fill_bytes(&mut nonce_bytes);
        let ciphertext;
        let nonce = GenericArray::from_slice(&nonce_bytes);
        {
            let d = self.key.borrow();
            let cipher = Aes256Gcm::new(GenericArray::from_slice(d.as_ref()));
    
            ciphertext = match cipher.encrypt(nonce, Payload {msg: &db_bytes, aad: &self.salt}) {
                Ok(v) => v,
                Err(e) => {
                    warn!("Encryption error! {:?}", e);
                    return false;
                }
            }; 
        }
        debug!("Done");
        debug!("Writing to file.");
        let metadata = Metadata::V1(self.salt, nonce_bytes);
        
        let file_data = PassFile{
                metadata,
                data: ciphertext
            };
        match bincode::serialize_into(into, &file_data) {
            Ok(()) => {
                debug!("Done.");
                true
            }
            Err(e) => {
                warn!(error = %e, "Could not write to file!");
                false
            }
        }
    }
    
    pub fn save(&mut self) -> bool {
        debug!(name = %self.file, "Writing data to file.");
        
        let mut file = match std::fs::create_dir_all("storage").and(File::create(format!("storage/{}", self.file))) {
            Ok(f) => f,
            Err(e) => {
                warn!("Could not create file! {:?}", e);
                return false;
            }
        };
        self.save_into(&mut file)
    }

    pub fn load(password: secrets::SecretVec<u8>, filename: String) -> Option<DB> {
        debug!(name = %filename, "Loading file,");
        let file = match File::open(format!("storage/{}", filename)) {
            Ok(f) => f,
            Err(e) => {
                warn!(error = %e, "Could not open file!");
                return None;
            }
        };
        let file_data: PassFile = match bincode::deserialize_from(file) {
            Ok(d) => d,
            Err(e) => {
                warn!(error = %e, "File is corrupt!");
                return None;
            }
        };
        debug!("Done.");
        match file_data.metadata {
            Metadata::V1(salt, nonce) => V1::load(password, salt, nonce, file_data.data, filename).map(|v| v.into())
        }
    }
    
    pub fn insert(&mut self, id: Option<u32>, site: Option<String>, uname: Option<String>, pwd:Option<String>, save: bool) -> Option<DBRow> {
        let id: u32 = if id.is_some() {id.unwrap()} else {rand::thread_rng().gen()};
        let newrow = DBRow::new(id, site, uname, pwd, &mut self.data.passwords);
        let clone = newrow.clone();
        self.data.rows.push(newrow);
        let res = if save {self.save()} else {true};
        if res {Some(clone)} else {None}
    }

    pub fn delete(&mut self, id: u32, save: bool) -> bool {
        let idx = self.data.rows.iter().position(|row| row.id == id);
        if idx.is_none() {
            return false;
        }
        self.data.rows.remove(idx.unwrap());
        if save {self.save()} else {true}
    }

    // term is lower case
    pub fn search(&self, term: &String) -> Vec<&DBRow> {
        if term == "" {return Vec::new()};
        let term = &term.to_lowercase();
        let mut found = Vec::new();
        for row in self.data.rows.iter() {
            let site = row.site.clone().map(|s| s.to_lowercase());
            let uname = row.uname.clone().map(|s| s.to_lowercase());
            if site.is_some() && site.clone().unwrap().contains(term){
                if site.clone().unwrap() == term.clone() {
                    found.push((0, row))
                } else if site.unwrap().starts_with(term) {
                    found.push((1, row))
                } else {
                    found.push((2, row))
                }
            }
            
            else if uname.is_some() && uname.clone().unwrap().contains(term){
                if uname.clone().unwrap() == term.clone() {
                    found.push((1, row))
                } else if uname.unwrap().starts_with(term) {
                    found.push((2, row))
                } else {
                    found.push((3, row))
                }
            }
        };
        found.sort_unstable_by(|a,b| a.0.cmp(&b.0));
        found.into_iter().map(|a| a.1).collect()
    }
}