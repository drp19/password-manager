use aes_gcm::{Aes256Gcm, NewAead, aead::{Aead, generic_array::GenericArray, Payload}};
use flate2::{read::DeflateDecoder};
use rand::Rng;
use secrets::{SecretBox, SecretVec};
use tracing::{debug, warn};
use serde::Deserialize;

pub(super) struct V1 {
    pub(super) data: V1DBData,
    pub(super) salt: [u8; 16],
    pub(super) key: SecretBox::<[u8; 32]>,
    pub(super) file: String
}

/// Stores actual data (stuff that should be encrypted)
#[derive(Deserialize)]
pub(super) struct V1DBData {
    pub(super) rows: Vec<V1DBRow>
}

/// A single database entry
#[derive(Deserialize)]
pub(super) struct V1DBRow {
    pub(super) id: u32,
    pub(super) site: Option<String>,
    pub(super) uname: Option<String>,
    pub(super) pwd: Option<Vec<u8>>
}

impl V1 {
    pub(super) fn new(password: secrets::SecretVec<u8>, salt: Option<[u8;16]>, file: String) -> V1 {
        debug!("Generating key");
        // salt is random, can be whatever really (doesn't need to be secure)
        let mut rng = rand::thread_rng();
        let salt = if salt.is_some() {salt.unwrap()} else {rng.gen::<u128>().to_le_bytes()};
    
        let mut key = SecretBox::<[u8;32]>::zero();
        {
            let mut d = key.borrow_mut();
            scrypt::scrypt(password.borrow().as_ref(), &salt, &scrypt::Params::new(16, 8, 1).unwrap(), d.as_mut()).unwrap();
        }
        debug!("Done");
        V1 {
            data: V1DBData { rows: Vec::new() },
            key,
            salt,
            file
        }
    
    }
    
    pub(super) fn load(password: SecretVec<u8>, salt: [u8;16], nonce_bytes: [u8;12], data: Vec<u8>, file: String) -> Option<V1> {
        debug!("Generating key");
        let mut key = SecretBox::<[u8; 32]>::zero();
        {
            let mut d = key.borrow_mut();
            scrypt::scrypt(password.borrow().as_ref(), &salt, &scrypt::Params::new(16, 8, 1).unwrap(), d.as_mut()).unwrap();
        }
        debug!("Done");
    
        debug!(bytes = data.len(), "Decrypting data.");
    
        let plaintext;
        let nonce = GenericArray::from_slice(&nonce_bytes);
        {
            let d = key.borrow();
            let cipher = Aes256Gcm::new(GenericArray::from_slice(d.as_ref()));
    
            plaintext = match cipher.decrypt(nonce, Payload {msg: data.as_ref(), aad: &salt}) {
                Ok(v) => v,
                Err(e) => {
                    warn!("Incorrect password.");
                    debug!(error = %e, "Encryption error.");
                    return None;
                }
            };
        }
        debug!("Done.");
    
        debug!("Unpacking data.");
        let decoder = DeflateDecoder::new(&plaintext[..]);
        let data: V1DBData = match bincode::deserialize_from(decoder) {
            Ok(v) => v,
            Err(e) => {
                warn!(error = %e, "Corrupted file or wrong type.");
                return None;
            }
        };
        debug!("Done.");
        Some(V1 {
            data,
            key,
            salt,
            file
        })
        
    }
}